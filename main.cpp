#include <iostream>
#include <ctime>

//wprowadzenie ile osob ma zostac dodanych do bazy
//mozliwosc przerwania dodawania osob  w dowolnym momencie
//mozliwosc zmiany danych wskazanej osoby (jej indeksu)
//mozliwosc usuniecia danych z indeksu (dodatkowe - nieobowiazkowe)
//menu, z ktorego mozna te wszystkie opcje wybrac (w tym zakonczyc
//program)
//wyswietlenie wszystkich danych badz wybranych

using namespace std;

bool sprawdz();
string sprawdzPlec(int wartosc);

int main() {
    int licznik=0, max_wartosc=1;
    int wiek[max_wartosc], plec[max_wartosc];
    string imie[max_wartosc], nazwisko[max_wartosc],
            miejsce_urodzenia[max_wartosc];
//    int wiek=0, plec=0;
//    string imie, nazwisko, miejsce_urodzenia;

    while(licznik<max_wartosc) {
        cout << "Podaj swoje imie oraz swoj wiek: ";
        do {
            cin >> imie[licznik] >> wiek[licznik];
            if(sprawdz()) break;
        } while(1);
        cout << 2018-wiek[licznik] << " - to Twoj rok urodzenia, " << imie[licznik] << endl;
        cout << "Podaj swoje nazwisko: ";
        cin >> nazwisko[licznik];
        cout << "Podaj miejsce urodzenia: ";
        cin >> miejsce_urodzenia[licznik];
        cout << "Podaj swoja plec (1-zenska,2-meska): ";

        do {
            cin >> plec[licznik];
            if(sprawdz()) break;
        } while(1);
        plec[licznik]--;
        licznik++;
    }
    for(int i=0; i <max_wartosc; i++) {
        cout << "Uzytkownik z identyfikatorem" << i+1 << endl;
        cout << "Imie: " << imie[i] << "\nNazwisko: " << nazwisko[i]
                << "\n Miejsce urodzenia: " << miejsce_urodzenia[i]
                   << "\nWiek: " << wiek[i] << "\nPlec: "
                   << sprawdzPlec(plec[i]) << endl;

    }
    return 0;
}

bool sprawdz() {
    if(!cin.fail()) return true;
    cout << "Wprowadzono bledne dane!\r\n";
    cout << "Najpierw wprowadz IMIE, pozniej WIEK: ";
    cin.clear();
    cin.ignore(256, '\n');
    return false;
}

string sprawdzPlec(int wartosc){
    string ret;
    if (wartosc==1)
        ret="mezczyzna";
    else
        ret="kobieta";
    return ret;
}
